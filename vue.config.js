module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "production-path" : "/",
  outputDir: "build",
  assetsDir: "assets"
};

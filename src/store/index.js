import Vue from "vue";
import Vuex from "vuex";
import ApiService from "@/core/api";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    allMovies: [],
    searchQuery: "",
    searchByTitle: true,
    sortByReleaseDate: true,
    searchResults: [],
    counter: 0,
    loading: false
  },
  getters: {
    allMovies(state) {
      return state.allMovies;
    },
    searchQuery(state) {
      return state.searchQuery;
    },
    searchResults(state) {
      return state.searchResults;
    },
    counter(state) {
      return state.searchResults.length;
    },
    loading(state) {
      return state.loading;
    }
  },
  mutations: {
    updateSearchQuery(state, query) {
      state.searchQuery = query;
    },
    setSearchByTitle(state, isSearchByTitle) {
      state.searchByTitle = isSearchByTitle;
    },
    setSortByReleaseDate(state, isSortByReleaseDate) {
      state.sortByReleaseDate = isSortByReleaseDate;
    },
    updateSearchResults(state, searchResults) {
      state.searchResults = searchResults;
    },
    sortSearchResults(state) {
      if (state.sortByReleaseDate) {
        sortByDate(state);
      } else {
        sortByRating(state);
      }
    },
    updateMoviesData(state, data) {
      state.allMovies = data;
    },
    setLoading(state, isLoading) {
      state.loading = isLoading;
    }
  },
  actions: {
    async search({ dispatch, commit, state }) {
      commit("setLoading", true);
      if (state.allMovies.length === 0) {
        await dispatch("fetchMovies");
      }
      let searchResults = [];
      if (state.searchByTitle) {
        searchResults = state.allMovies.filter(movie =>
          movie.title.includes(state.searchQuery)
        );
      } else {
        searchResults = state.allMovies.filter(movie =>
          movie.genre.includes(state.searchQuery)
        );
      }
      commit("updateSearchResults", searchResults);
      commit("sortSearchResults");
      commit("setLoading", false);
    },
    fetchMovies({ commit }) {
      return ApiService.fetchMovies().then(movies =>
        commit("updateMoviesData", movies)
      );
    }
  },
  modules: {}
});

const sortByDate = function(state) {
  state.searchResults.sort((a, b) => {
    return new Date(a.releaseDate) - new Date(b.releaseDate);
  });
};

const sortByRating = function(state) {
  state.searchResults.sort((a, b) => {
    return a.rating - b.rating;
  });
};

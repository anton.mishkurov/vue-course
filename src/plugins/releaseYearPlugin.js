const ReleaseYearPlugin = {
  install(Vue) {
    Vue.filter("releaseYear", value => new Date(value).getFullYear());
  }
};

export default ReleaseYearPlugin;

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ApiService from "@/core/api";
import ReleaseYearPlugin from "@/plugins/releaseYearPlugin";

Vue.config.productionTip = false;

Vue.use(ReleaseYearPlugin);

ApiService.init();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

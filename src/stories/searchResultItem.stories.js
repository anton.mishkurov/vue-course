import SearchResultItem from "../components/SearchResultItem.vue";
import { text, withKnobs } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import movieCover from "./../assets/KillBillCover.jpg";

export default {
  title: "Search Result Item",
  component: SearchResultItem,
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const movieItemData = {
  image: movieCover,
  title: text("Title", "Bohemian Rhapsody"),
  genre: text("Genre", "Drama"),
  releaseDate: text("Release Date", "2003")
};

export const item = () => ({
  components: { SearchResultItem },
  template:
    '<search-result-item :item=item @search-result-item-click="clickOnItem"/>',
  methods: { clickOnItem: action("clicked") },
  props: {
    item: {
      default: movieItemData
    }
  }
});

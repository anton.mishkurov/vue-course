import SearchResults from "../components/SearchResults.vue";
import { movieItemData } from "./searchResultItem.stories";

export default {
  title: "Search results",
  component: SearchResults,
  excludeStories: /.*Data$/
};

export const withEmptyResults = () => ({
  components: { SearchResults },
  template: '<search-results :items="items" />',
  methods: {},
  props: {
    items: {
      default: () => []
    }
  }
});

export const withLoadingResults = () => ({
  components: { SearchResults },
  template: '<search-results :items="items" :loading="true" />',
  methods: {},
  props: {
    items: {
      default: () => []
    }
  }
});

export const withResults = () => ({
  components: { SearchResults },
  template: '<search-results :items="items" />',
  methods: {},
  props: {
    items: {
      default: () => [
        movieItemData,
        movieItemData,
        movieItemData,
        movieItemData
      ]
    }
  }
});

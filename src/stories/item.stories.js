import { action } from "@storybook/addon-actions";
import Item from "../components/Item.vue";
import { withKnobs } from "@storybook/addon-knobs";
import movieCover from "./../assets/KillBillCover.jpg";

export default {
  title: "Movie",
  component: Item,
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const actionsData = {
  openSearchPage: action("goto-search")
};

export const itemData = {
  img: movieCover,
  title: "Movie title",
  rating: "9.9",
  releaseDate: "2000",
  duration: "154",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore " +
    "magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo " +
    "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
    "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est " +
    "laborum.",
  genre: "Action"
};

export const withMovie = () => ({
  components: { Item },
  template: '<item @goto-search-page="openSearchPage" :item="item"/>',
  methods: actionsData,
  props: {
    item: {
      default: () => itemData
    }
  }
});

import { action } from "@storybook/addon-actions";
import SearchResultsHeader from "../components/SearchResultsHeader.vue";
import { withKnobs } from "@storybook/addon-knobs";

export default {
  title: "Search results header",
  component: SearchResultsHeader,
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const actionsData = {
  sortResults: action("sort-search-results")
};

export const withSortByReleaseDate = () => ({
  components: { SearchResultsHeader },
  template:
    '<search-results-header @sort-results="sortResults" :config="config" :counter="counter"/>',
  methods: actionsData,
  props: {
    config: {
      default: () => ({
        sortByReleaseDate: true
      })
    }
  }
});

export const withSortByRating = () => ({
  components: { SearchResultsHeader },
  template:
    '<search-results-header @sort-results="sortResults" :config="config" :counter="counter"/>',
  methods: actionsData,
  props: {
    config: {
      default: () => ({
        sortByReleaseDate: false
      })
    }
  }
});

export const withCounter = () => ({
  components: { SearchResultsHeader },
  template:
    '<search-results-header @sort-results="sortResults" :config="config" :counter="counter"/>',
  methods: actionsData,
  props: {
    config: {
      default: () => ({
        sortByReleaseDate: false
      })
    },
    counter: {
      default: 123
    }
  }
});

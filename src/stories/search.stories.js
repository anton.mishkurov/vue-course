import { action } from "@storybook/addon-actions";
import Search from "../components/Search.vue";
import { withKnobs } from "@storybook/addon-knobs";

export default {
  title: "Search",
  component: Search,
  decorators: [withKnobs],
  excludeStories: /.*Data$/
};

export const actionsData = {
  startSearch: action("start-search")
};

export const searchByTitle = () => ({
  components: { Search },
  template: '<search @start-search="startSearch" :config="config"/>',
  methods: actionsData,
  props: {
    config: {
      default: () => ({
        searchByTitle: true
      })
    }
  }
});

export const searchByGenre = () => ({
  components: { Search },
  template: '<search @start-search="startSearch" :config="config"/>',
  methods: actionsData,
  props: {
    config: {
      default: () => ({
        searchByTitle: false
      })
    }
  }
});

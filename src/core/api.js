import axios from "axios";

const ApiService = {
  init() {
    axios.defaults.baseURL = "http://react-cdp-api.herokuapp.com/";
  },

  fetchMovies() {
    return axios
      .get("movies")
      .then(response => response.data.data)
      .then(movies =>
        movies.map(movie => ({
          id: movie.id,
          image: movie.poster_path,
          title: movie.title,
          genre: movie.genres[0],
          rating: movie.vote_average,
          releaseDate: movie.release_date,
          duration: movie.runtime || "",
          description: movie.overview
        }))
      );
  }
};

export default ApiService;
